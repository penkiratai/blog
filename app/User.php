<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Post;
use App\Comment;
use Carbon\Carbon;
use Session;

class User extends Authenticatable
{
    use Notifiable;

    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function createPost($data) {
        $post = new Post();
        
        $post->title = $data['title'];
        $post->image = $data['image'];
        $post->text = $data['text'];
        $post->date = Carbon::now();
        $post->author = $this->name;
        if ($data['location'] != '') {
            $post->location = $data['location'];
        }

        $post->save();

        return $post;
    }

    public function deleteOwnPost($id) {
        Comment::where('post_id', $id)->delete();
        $post = Post::find($id);
        $post->delete();
    }

    public function createComment($data) {
        $comment = new Comment();
        
        $comment->text = $data['text'];
        $comment->date = Carbon::now();
        $comment->author = $this->name;
        $comment->post_id = $data['post_id'];

        $comment->save();

        return $comment;
    }

    public function deleteOwnComment($id) {
        $comment = Comment::find($id);
        $comment->delete();
    }

    public function logout() {
        Session::flush();
    }
}