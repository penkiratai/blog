<?php

namespace App;

use App\User;
use App\Post;
use App\Comment;

class Admin {
    public static function deleteComment($id) {
        $comment = Comment::find($id);
        $comment->delete();
    }

    public static function deletePost($id) {
        $post = Post::find($id);
        Comment::where('post_id', $id)->delete();
        $post = Post::find($id);
        $post->delete();
    }

    public static function deleteUser($name) {
        $user = User::where('name', $name)->first();
        Comment::where('author', $name)->delete();
        Post::where('author', $name)->delete();
        $user->delete();
    }
}