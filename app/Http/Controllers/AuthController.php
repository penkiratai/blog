<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Http\Request;
use App\User;
use App\Admin;
use App\Post;
use App\Comment;
use Auth;

class AuthController extends Controller {
    public function showRegPage()
    {
        return view('register');
    }

    public function showLogPage() 
    {
        return view('login', ['error' => false]);
    }

    public function showSettings() {
        return view('settings');
    }

    public function login(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');
        if (Auth::attempt(['name' => $username, 'password' => $password])) {
            return redirect('/');
        } else {
            return view('login', ['error'=> true, 'message' => 'Your username/password is doesn\'t match']);
        }
    }

    public function register(Request $request) 
    {   

        $this->validate($request, [
            'username' => 'required|alpha_num|unique:users,name|max:25',
            'email' => 'required|email|max:255',
            'password' => 'required|max:255'
        ]);

        $attributes = array(
            'username' => $request->input('username'),
            'email' => $request->input('email'),
            'password' => $request->input('password')
        );

        $user = $this->create($attributes);
        Auth::guard()->login($user);

        return redirect('/');
    }

    public function logoutUser()
    {
        Auth::user()->logout();
        return redirect('/');
    }

    public function changeRole($name) {
        $user = User::where('name', $name)->first();

        $user->admin = !$user->admin;

        $user->save();

        return back();
    }

    public function updateUser(Request $request, $name) {
        $user = User::where('name', $name)->first();

        $this->validate($request, [
            'email' => 'required|email',
            'password' => ''
        ]);

        $user->email = $request->email;
        if ($request->password) {
            $user->password = bcrypt($request->password);
        }

        $user->save();

        return back();
    }

    public function deleteUser($name) {
        if (Auth::user()->admin) {
            Admin::deleteUser($name);
            return back();
        } else {
            return redirect('/');
        }
    }

    protected function create(array $data)
    {
        return User::create([
            'name' => $data['username'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'admin' => false,
        ]);
    }
}