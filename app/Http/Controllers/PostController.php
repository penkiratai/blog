<?php

namespace App\Http\Controllers;

use App\Post;
use App\Comment;
use App\User;
use App\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class PostController extends Controller {

    public function showIndex()
    {
        $posts = Post::all();

        return view('main', ['posts' => $posts]);
    }

    public function showPost($id) 
    {
        $post = Post::find($id);
        $comments = Comment::where('post_id', $id)->orderBy('date', 'desc')->get();

        return view('post', ['post' => $post, 'comments' => $comments]);
    }

    public function showManagePosts() {
        $posts = Post::orderBy('date', 'desc')->get();

        return view('managePosts', ['posts' => $posts]);
    }

    public function showManageUsers() {
        $users = User::orderBy('name')->get();

        return view('manageUsers', ['users' => $users]);
    }

    public function showEditPost($id) {
        $post = Post::find($id);
        return view('editPost', ['post' => $post]);
    }

    public function searchPost(Request $request) {

        $this->validate($request, [
            'search' => 'required|max:255|alpha_num'
        ]);

        $posts = Post::where('title', 'LIKE', '%'.$request->search.'%')->get();

        return view('search', ['posts' => $posts]);
    }

    public function createPost(Request $request)
    {

        $this->validate($request, [
            'title' => 'required|max:255',
            'image' => 'image|nullable',
            'text' => 'required|max:2000',
            'location' => 'string|nullable|max:255'
        ]);

        $data = array(
            'title' => $request->title,
            'text' => $request->text,
            'location' => $request->location,
            'image' => ''
        );

        if ($request->hasFile('image')) {
            $data['image'] = basename($request->image->store('public/images'));
        }
        
        $post = Auth::user()->createPost($data);

        return redirect('/post/'.$post->id);
    }

    public function updatePost(Request $request, $id) {
        $post = Post::find($id);

        $this->validate($request, [
            'title' => 'required|max:255',
            'image' => 'image|nullable',
            'text' => 'required',
            'location' => 'string|nullable|max:255'
        ]);

        if ($request->hasFile('image')) {
            $img = basename($request->image->store('public/images'));
            $post->image = $img;
        }

        $post->title = $request->title;
        $post->text = $request->text;
        $post->location = $request->location;

        $post->save();

        return redirect('/post/'.$id);
    }

    public function deletePost($id) {
        if (Auth::user()->admin) {
            Admin::deletePost($id);
        } else {
            Auth::user()->deleteOwnPost($id);
        }
        return back();
    }

    public function showNewPost() 
    {
        return view('newPost');
    }

    public function showProfile($name = null) {
        if ($name) {
            $user = User::where('name', $name)->first();
            $posts = Post::where('author', $name)->orderBy('date', 'desc')->get();
            $comments = Comment::where('author', $name)->orderBy('date', 'desc')->get();
        } else {
            $user = Auth::user();
            $posts = Post::where('author', Auth::user()->name)->orderBy('date', 'desc')->get();
            $comments = Comment::where('author', Auth::user()->name)->orderBy('date', 'desc')->get();
        }

        return view('profile', ['user' => $user, 'posts' => $posts, 'comments' => $comments]);
    }

    public function createComment(Request $request)
    {
        
        $this->validate($request, [
            'comment' => 'required',
            'postId' => 'required|integer'
        ]);

        $data = array(
            'text' => $request->comment,
            'post_id' => $request->postId
        );

        $comment = Auth::user()->createComment($data);

        return redirect('/post/'.$request->postId);
    }

    public function deleteComment($id) {
        if (Auth::user()->admin) {
            Admin::deleteComment($id);
        } else {
            Auth::user()->deleteOwnComment($id);
        }
        return back();
    }
}

