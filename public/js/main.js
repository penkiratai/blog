function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
}

function handleLocation(checkbox) {
    var location = '';  
    if (checkbox.checked) {
        if (location == '') {
            getCoordinates(function (latitude, longitude) {
                var gAPIreq = new XMLHttpRequest();
                gAPIreq.onreadystatechange = function(){
                    if (gAPIreq.readyState === XMLHttpRequest.DONE) {
                        if (gAPIreq.status === 200) {
                            location = JSON.parse(gAPIreq.responseText).results[0].formatted_address;
                            document.getElementById('location').value = location;
                        } else {
                            // throw 500 error
                        }
                    }
                };
                gAPIreq.open('GET', 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+latitude+','+longitude+'&key=AIzaSyDdLBSJuzhAqT-qcvdqy0iHUGCXmULIA0g');
                gAPIreq.send();
            });
        } else {
            document.getElementById('location').value = location;
        }
    } else {
        document.getElementById('location').value = '';
    }
}

function getCoordinates(callback) {
    var apiURL = 'https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyAWfxry8CrteGMFAeKaNEL5SNTTYGCecOU';
    var req = new XMLHttpRequest();

    req.onreadystatechange = function(){
        if (req.readyState === XMLHttpRequest.DONE) {
            if (req.status === 200) {
                var coords = JSON.parse(req.responseText).location;
                callback(coords.lat, coords.lng);
            } else {
                // throw 500 error
            }
        }
    };
    req.open('POST', apiURL);
    req.send();
}

