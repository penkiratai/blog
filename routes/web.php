<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PostController@showIndex');

Route::get('/post', 'PostController@showNewPost')->middleware('auth');
Route::post('/post', 'PostController@createPost')->middleware('auth');
Route::get('/post/{id}', 'PostController@showPost');
Route::post('/post/{id}/comment', 'PostController@createComment')->middleware('auth');
Route::get('/post/{id}/delete', 'PostController@deletePost')->middleware('auth');
Route::get('/post/{id}/edit', 'PostController@showEditPost')->middleware('auth');
Route::post('/post/{id}/update', 'PostController@updatePost')->middleware('auth');
Route::get('/comment/{id}/delete', 'PostController@deleteComment')->middleware('auth');
Route::post('/search', 'PostController@searchPost');

Route::get('/profile/{name?}', 'PostController@showProfile')->middleware('auth');
Route::get('/profile/{name}/admin', 'AuthController@changeRole')->middleware(['auth', 'admin']);
Route::get('/profile/{name}/delete', 'AuthController@deleteUser')->middleware(['auth', 'admin']);
Route::post('/profile/{name}/update', 'AuthController@updateUser')->middleware('auth');

Route::get('/settings', 'AuthController@showSettings')->middleware('auth');

Route::get('/admin/posts', 'PostController@showManagePosts')->middleware(['auth', 'admin']);
Route::get('/admin/users', 'PostController@showManageUsers')->middleware(['auth', 'admin']);

Route::get('/register', 'AuthController@showRegPage')->name('register');
Route::post('/register', 'AuthController@register');
Route::get('/login', 'AuthController@showLogPage')->name('login');
Route::post('/login', 'AuthController@login');
Route::get('/logout', 'AuthController@logoutUser');