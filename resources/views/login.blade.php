@extends('layouts.app')
@section('content')
<div class="col-lg-5 col-md-5">
    @if ($error)
    <div class="alert alert-danger" role="alert">{{ $message }}</div>
    @endif
    <form action="/login" method="post">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="username">Username</label>
            <input name="username" type="text" class="form-control" id="username" placeholder="Username" required>
        </div>
        <div class="form-group">
            <label for="password">Password</label>
            <input name="password" type="password" class="form-control" id="password" placeholder="Password" required>
        </div>
        <input type="submit" class="btn btn-default" value="Submit">
    </form>
</div>
@endsection