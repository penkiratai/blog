<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Blog</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="{{ asset('/css/main.css') }}" rel="stylesheet">
    </head>
    <body>
    <div id="mySidenav" class="sidenav">
      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
      @guest
      <a href="/login">Login</a>
      <a href="/register">Register</a>
      @endguest
      @auth
      <a href="/post">New Post</a>
      <a href="/profile">Profile</a>
      <a href="/settings">Settings</a>
      <a href="/logout">Logout</a>
      @if (Auth::user()->admin)
      <h3>Admin</h3>
      <a href="/admin/posts">Manage posts</a>
      <a href="/admin/users">Manage users</a>
      @endif
      @endauth
      <form method="post" action="/search" class="navbar-form navbar-right">
        {{ csrf_field() }}
        <div class="form-group left-search">
          <input type="text" class="form-control" name="search" placeholder="Search..">
        </div>
      </form>
    </div>

    <div id="main">
      <div class="myNav">
        <span style="font-size:30px;cursor:pointer; color:white" onclick="openNav()">&#9776;</span>
      </div>
      <div class="content">
        <div class="header">
          <a href="/"><img id="logo" src="http://icons.iconarchive.com/icons/turbomilk/animals/256/donkey-icon.png"></a>
          <ul class='nav navbar-nav navbar-right'>
            @auth
            <li id='user'><a class='account'href="/profile">Welcome, {{ Auth::user()->name }}</a></li>
            @endauth
          </ul>

        </div>


        @yield('content')





      </div>
    </div>
    <script src="{{ asset('/js/main.js') }}"></script>
    </body>
</html>
