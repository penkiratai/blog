@extends('layouts.app')
@section('content')
<div class="col-lg-5 col-md-5">
    <h1>Settings</h1>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="/profile/{{ Auth::user()->name }}/update" method="post">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="email">Email address</label>
            <input name="email" type="email" class="form-control" id="email" placeholder="Email" value="{{ Auth::user()->email }}" required>
        </div>
        <div class="form-group">
            <label for="password">New Password</label>
            <input name="password" type="password" class="form-control" id="password" placeholder="Password">
        </div>
        <input type="submit" class="btn btn-default" value="Submit">
    </form>
</div>
@endsection
