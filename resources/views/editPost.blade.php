@extends('layouts.app')
@section('content')
<div class="col-lg-6 col-md-10">
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <form action="/post/{{ $post->id }}/update" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="title">Title</label>
            <input name="title" type="text" class="form-control" id="title" placeholder="Title" value="{{ $post->title }}" required>
        </div>
        <div class="form-group">
            <label for="text">Text</label>
            <textarea class="form-control" rows="5" name="text" placeholder="Your text...">{{ $post->text }}</textarea>
        </div>
        <div class="form-group">
            <label for="image">Image</label>
            <input type="file" id="image" name="image" accept=".jpg, .jpeg, .png">
            <p class="help-block">Accepts JPG and PNG images.</p>
        </div>
        <div class="form-group">
        <label for="location" class="control-label">Location</label>
        </div>
        <div class="form-group col-sm-10 no-padding">
            <input class="form-control" type="text" name="location" id="location" value="{{ $post->location }}" placeholder="Location" readonly>
        </div>
        <div class="checkbox col-sm-2">
            <label>
            <input type="checkbox" onclick="handleLocation(this)" @if ($post->location) checked @endif> Show your location?
            </label>
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-default" value="Submit">
        </div>
    </form>
</div>
@endsection