@extends('layouts.app')
@section('content')
<div class="col-lg-12 col-md-12">
  <div class="thumbnail">
    <div class="row">
      <div class="col-md-5">
        <div class="image-responsive">
            <img src="{{ asset('storage/images/'.$post->image) }}" alt="">
        </div>
      </div>
      <div class="col-md-7">
        <div class="caption">
          @if (Auth::user() && ($post->author == Auth::user()->name || Auth::user()->admin))<span class="pull-right"><a href="/post/{{ $post->id }}/edit">Edit</a></span>@endif
          <h4>{{ $post->title }}</h4>
          <h5>{{ $post->date }} | {{ $post->author }}</h5>
          @if (!empty($post->location))
          <h5>Location: {{ $post->location }}</h5>
          @endif
            <p>{{ $post->text }}</p>
        </div>
      </div>
    </div>
  </div>

  @auth
  <div class="container">
    <h4>Submit your comment</h4>
    <form action="{{ url()->current() }}/comment" method="post">
        {{ csrf_field() }}
        <div class="form-group">
          <textarea class="form-control" rows="5" name="comment" placeholder="Your comment..."></textarea>
          <input type="text" name="postId" class="hidden" value="{{ $post->id }}">
        </div>
        <input type="submit" class="btn btn-default" value="Submit">
    </form>
  </div>
  @endauth

  <div class="container">
    <h4>Comments</h4> 
  @foreach ($comments as $comment)
    <div class="panel panel-default">
      <div class="panel-heading">{{ $comment->author }} <span class="pull-right">{{ $comment->date }} @if (Auth::user() && ($comment->author == Auth::user()->name || Auth::user()->admin))| <a href="/comment/{{ $comment->id }}/delete">Delete</a>@endif</span></div>
      <div class="panel-body">
        {{ $comment->text }}
      </div>
    </div>
  @endforeach
  </div>
</div>
@endsection