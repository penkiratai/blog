@extends('layouts.app')
@section('content')
<div class="col-lg-12 col-md-12">
    <h1>Manage users</h1>
    @if($users->isEmpty())
        <h4 class="empty-title">There are no users</h4>
        @endif
        @foreach ($users as $user)
        <div class="panel panel-default">
            <div class="panel-body">
                <a href="/profile/{{ $user->name }}">{{ $user->name }}</a><span class="pull-right"><a href="/profile/{{ $user->name }}/admin">{{ $user->admin ? 'Change to user' : 'Change to admin' }}</a> | <a href="/profile/{{ $user->name }}/delete">Delete</a></span>
            </div>
        </div>
        @endforeach
</div>
@endsection