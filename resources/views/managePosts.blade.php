@extends('layouts.app')
@section('content')
<div class="col-lg-12 col-md-12">
    <h1>Manage posts</h1>
    @if($posts->isEmpty())
        <h4 class="empty-title">There are no posts</h4>
        @endif
        @foreach ($posts as $post)
        <div class="panel panel-default">
            <div class="panel-body">
                <a href="/post/{{ $post->id }}">{{ $post->title }}</a><span class="pull-right">{{ $post->date }} | <a href="/post/{{ $post->id }}/delete">Delete</a></span>
            </div>
        </div>
        @endforeach
</div>
@endsection