@extends('layouts.app')
@section('content')
<div class="col-lg-12 col-md-12">
@foreach ($posts as $post)
  <div class="thumbnail">
    <div class="row">
      <div class="col-md-5">
        <div class="image-responsive">
          <img src="{{ asset('storage/images/'.$post->image) }}" alt="">
        </div>
      </div>
      <div class="col-md-7">
        <div class="caption">
          <h4><a href="{{ url('post/'.$post->id) }}">{{ $post->title }}</a></h4>
          <h5>{{ $post->date }} | {{ $post->author }}</h5>
          @if (!empty($post->location))
          <h5>Location: {{ $post->location }}</h5>
          @endif
          <p>{{ $post->text }}</p>
        </div>
      </div>
    </div>
  </div>
@endforeach
</div>
@endsection