@extends('layouts.app')
@section('content')
<div class="col-lg-12 col-md-12">
    <h1>{{ $user->name }} <span class="empty-title small">{{$user->email}}</span></h1>

    <div class="col-lg-8 col-sm-12">
        <h3>Posts</h3>
        @if($posts->isEmpty())
        <h4 class="empty-title"> You have no posts :(</h4>
        @endif
        @foreach ($posts as $post)
        <div class="panel panel-default">
            <div class="panel-body">
                <a href="/post/{{ $post->id }}">{{ $post->title }}</a><span class="pull-right">{{ $post->date }}@if ($post->author == Auth::user()->name || Auth::user()->admin) | <a href="/post/{{ $post->id }}/edit">Edit</a> | <a href="/post/{{ $post->id }}/delete">Delete</a>@endif</span>
            </div>
        </div>
        @endforeach
    </div>
    <div class="col-lg-4 col-sm-12">
        <h3>Comments</h3>
        @if($comments->isEmpty())
        <h4 class="empty-title"> You have no comments :(</h4>
        @endif
        @foreach ($comments as $comment)
        <div class="panel panel-default">
            <div class="panel-heading">{{ $comment->author }} <span class="pull-right">{{ $comment->date }}@if ($comment->author == Auth::user()->name || Auth::user()->admin) | <a href="/comment/{{ $comment->id }}/delete">Delete</a>@endif</span></div>
            <div class="panel-body">
                {{ $comment->text }}
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection